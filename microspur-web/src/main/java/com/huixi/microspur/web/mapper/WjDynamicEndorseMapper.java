package com.huixi.microspur.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huixi.microspur.web.pojo.entity.dynamic.WjDynamicEndorse;

/**
 *  动态点赞表 Mapper 接口
 * @Author 叶秋 
 * @Date 2020/4/20 22:00
 * @param 
 * @return 
 **/
public interface WjDynamicEndorseMapper extends BaseMapper<WjDynamicEndorse> {
}
