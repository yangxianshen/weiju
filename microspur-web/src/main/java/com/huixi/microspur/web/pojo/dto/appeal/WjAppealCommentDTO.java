package com.huixi.microspur.web.pojo.dto.appeal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 *  单独一个接受前端 传过来的 评论值
 * @Author 叶秋 
 * @Date 2020/6/12 0:05
 * @param 
 * @return 
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "诉求评论值", description = "诉求评论需要的值")
public class WjAppealCommentDTO {


    @ApiModelProperty(value = "是否是评价的评论（这里要写被评论的id，写了就是，没写就是普通的评论）")
    private String aboutComment;

    @NotNull(message = "诉求id为空")
    @ApiModelProperty(value = "诉求id",required = true)
    private String appealId;

    @ApiModelProperty(value = "评论人的id", hidden = true)
    private String userId;

    @NotNull(message = "评论的内容为空")
    @ApiModelProperty(value = "评论的内容", required = true)
    private String content;

    @ApiModelProperty(value = "评论所加的贴图（只能是一张）")
    private String url;


}
