package com.huixi.microspur.web.pojo.vo.appeal;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 *  这个类与 QueryAppealVO 类 多了存储了前三位评论人的头像，但是为了严格区分 重新建一个类
 * @Author 叶秋
 * @Date 2020/8/5 20:53
 * @param
 * @return
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class QueryAppealByIdVo extends QueryAppealVO{

    @ApiModelProperty(value = "评论用户的头像资源地址")
    private List<String > commentUserHeadPortrait;

}
