package com.huixi.microspur.commons.page;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 分页查询的请求参数封装
 *
 * @author 碧海青天夜夜心
 * @sice 2019年10月15日 16:05:17
 */
@Data
@ApiModel(value = "接收前端分页参数的封装")
public class PageQuery {

    /**
     * 每页的条数
     */
    @ApiModelProperty(value = "每页的条数")
    private Integer pageSize;

    /**
     * 页编码(第几页)
     */
    @ApiModelProperty(value = "页编码")
    private Integer pageNo;


    public PageQuery() {
    }

    public PageQuery(Integer pageSize, Integer pageNo, String sort, String orderByField) {
        this.pageSize = pageSize;
        this.pageNo = pageNo;
    }
}
