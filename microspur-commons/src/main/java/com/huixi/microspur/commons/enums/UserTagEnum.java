package com.huixi.microspur.commons.enums;

/**
 *  用户的标签
 * @Author 叶秋
 * @Date 2020/7/2 23:27
 * @param
 * @return
 **/
public enum  UserTagEnum {

    USER_TAG_100(100,"开发者"),
    USER_TAG_101(101, "开源参与者"),
    USER_TAG_102(102, "用户"),
    USER_TAG_103(103, "站长"),
    USER_TAG_2333(2333, "彩蛋🤗")

    ;

    private Integer id;
    private String value;

    UserTagEnum(Integer id ,String value) {
        this.id = id;
        this.value = value;
    }

    public String value() {
        return this.value;
    }

    public Integer id() {
        return this.id;
    }


    public static UserTagEnum byIdTagValue(Integer id){

        UserTagEnum[] values = UserTagEnum.values();
        for (UserTagEnum value : values) {
            if(id.equals(value.id)){
                return value;
            }
        }

        return UserTagEnum.USER_TAG_2333;

    }


}
