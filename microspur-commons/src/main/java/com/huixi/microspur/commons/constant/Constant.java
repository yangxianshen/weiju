package com.huixi.microspur.commons.constant;

/**
 * 常量
 */
public interface Constant {
    /**
     * 用户标识
     */
    int SUCCESS = 1;
    /**
     * 失败
     */
    int FAIL = 0;
    /**
     * OK
     */
    String OK = "OK";
    /**
     *  升序
     */
    String ASC = "asc";
    /**
     * 降序
     */
    String DESC = "desc";
    /**
     * 删除字段名
     */
    String DEL_FLAG = "del_flag";
    /**
     * 创建时间字段名
     */
    String CREATE_DATE = "create_time";
    /**
     * 当前页码
     */
    String PAGE = "page";
    /**
     * 每页显示记录数
     */
    String LIMIT = "limit";
    /**
     * 排序字段
     */
    String ORDER_FIELD = "orderField";
    /**
     * 排序方式
     */
    String ORDER = "order";
    /**
     * token header
     */
    String TOKEN_HEADER = "token";

    /**
     *  删除标识 N 没有删除
     * @Author 叶秋
     * @Date 2020/5/5 11:04
     **/
    String NORMAL = "N";

    /**
     *  删除标识 Y 标识已经删除
     * @Author 叶秋
     * @Date 2020/5/5 11:04
     **/
    String DEL = "Y";


    String USER_KEY = "sessionKey";


    // 阿里oss 上传文件相关

    /**
     *  阿里oss 测试域名
     **/
    String ALIOSSDOMAINNAME_TEST = "https://weiju1.oss-cn-shenzhen.aliyuncs.com/";

    /**
     *  阿里OSS 正式域名
     **/
    String ALIOSSDOMAINNAME = "https://weiju-wechat.oss-cn-shenzhen.aliyuncs.com/";

    /**
     * 诉求模块
     **/
    String APPEAL_MATERIAL = "appeal";

    /**
     * 动态模块
     **/
    String DYNAMIC_MATERIAL = "dynamic";




}