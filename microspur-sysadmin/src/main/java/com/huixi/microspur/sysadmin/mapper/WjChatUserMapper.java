package com.huixi.microspur.sysadmin.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huixi.microspur.sysadmin.pojo.entity.chat.WjChatUser;

/**
 * <p>
 * 聊天室对应的用户 Mapper 接口
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjChatUserMapper extends BaseMapper<WjChatUser> {

}
