package com.huixi.microspur.sysadmin.pojo.dto.user;

import lombok.Data;

import java.io.Serializable;

/**
 *  发送code 给微信后端 给的session_key 值。（没有用户的敏感信息）
 * @Author 李辉 
 * @Date 2019/11/21 23:32
 * @param 
 * @return 
 **/
@Data
public class WxSesssionKeyDTO implements Serializable {

    private String openid;

    private String session_key;

}
