package com.huixi.microspur.sysadmin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.sysadmin.pojo.entity.saying.WjSaying;

/**
 * <p>
 * 语录表 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjSayingService extends IService<WjSaying> {

}
