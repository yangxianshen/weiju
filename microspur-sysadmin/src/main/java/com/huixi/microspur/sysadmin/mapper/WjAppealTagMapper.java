package com.huixi.microspur.sysadmin.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huixi.microspur.sysadmin.pojo.entity.appeal.WjAppealTag;

/**
 * <p>
 * 诉求-对应标签 Mapper 接口
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjAppealTagMapper extends BaseMapper<WjAppealTag> {

}
