package com.huixi.microspur.sysadmin.pojo.vo.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 *  用户的一些数据
 * @Author 叶秋
 * @Date 2020/6/23 0:42
 * @param
 * @return
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UserDataVO {

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "昵称")
    private String nickName;

    @ApiModelProperty(value = "头像对应的URL地址")
    private String headPortrait;

    @ApiModelProperty(value = "性别 改用String，活泼一点。自定义都可以")
    private String sex;

    @ApiModelProperty(value = "电子邮箱")
    private String email;

    @ApiModelProperty(value = "个性签名（冗余）")
    private String signature;

    @ApiModelProperty(value = "简介")
    private String introduce;


    @ApiModelProperty(value = "自己发送诉求的次数")
    private Integer appealCount;

    @ApiModelProperty(value = "自己发送动态的数量")
    private Integer dynamicCount;

    @ApiModelProperty(value = "被对方点赞的次数")
    private Integer endorseCount;


}
