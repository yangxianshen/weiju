package com.huixi.microspur.sysadmin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.sysadmin.mapper.WjAppealEndorseMapper;
import com.huixi.microspur.sysadmin.pojo.entity.appeal.WjAppealEndorse;
import com.huixi.microspur.sysadmin.service.WjAppealEndorseService;
import org.springframework.stereotype.Service;

/**
 *  诉求点赞记录表的 服务实现类
 * @Author 叶秋 
 * @Date 2020/3/15 15:26
 * @param 
 * @return 
 **/
@Service
public class WjAppealEndorseServiceImpl extends ServiceImpl<WjAppealEndorseMapper, WjAppealEndorse> implements WjAppealEndorseService {


    @Override
    public Boolean isEndorse(String appealId, String userId) {

        QueryWrapper<WjAppealEndorse> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("appeal_id", appealId).eq("user_id", userId);
        int count = count(objectQueryWrapper);
        if(count!=0){
            return true;
        }

        return false;
    }

    @Override
    public int getTotalCount(String appealId) {
        QueryWrapper<WjAppealEndorse> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("appeal_id", appealId);

        return count(objectQueryWrapper);
    }




}
